# migrazione-dropbox-a-git

Script di migrazione dei laboratori dal vecchio sistema di gestione script a git

# Istruzioni d'uso
```
cd /opt
wget https://gitlab.com/openscuola/migrazione-dropbox-a-git/-/raw/master/convert_to_git.sh
chmod 744 ./convert_to_git.sh
./convert_to_git.sh
```

## Authors and acknowledgment
©2022 Massimo Gismondi

## License
This project is licensed with a AGPLv3 license or later. See the COPYING file for more information.

## Project status
These scripts won't be probably updated in the long run
