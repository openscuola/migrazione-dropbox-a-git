#!/bin/bash

# Copyright ©2022 Massimo Gismondi

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

################################################################################
######################## Controllo dei permessi di root ########################
################################################################################

if ! [ $(id -u) = 0 ]; then
  echo "Sono necessari i permessi di root!"
  exit 1
fi


# Vecchia configurazione
old_script_dir="/opt/openscuola"

################################################################################
################################ Configurazione ################################
################################################################################


# Nuova configurazione
# Cartella generale openscuola, con sottocartelle script e modelli
openscuola_main_dir="/opt/openscuola"
# Cartella con i soli script OpenScuola
script_dir="${openscuola_main_dir}/lab-configuration-scripts"
# Cartella modelli Libreoffice
libreoffice_dir="${openscuola_main_dir}/modelli-libreoffice"
# Cartella configurazione. Qui verranno salvati i file di configurazione laboratorio
config_dir="${openscuola_main_dir}/config"



################################################################################
######################## Verifico inizializzazione #############################
################################################################################
# Per lanciare correttamente lo script di migrazione, il laboratorio
# deve già essere stato inizializzato, ossia gli utenti docente e studentiXX
# devono già esistere.
# Controllo se l'utente docente esiste già
if getent passwd docente > /dev/null 2>&1; then
    echo "Laboratorio già inizializzato, proseguo."
else
    echo "L'utente docente non esiste."
    echo "Inizializzare il laboratorio prima di eseguire la migrazione"
    exit 1
fi



################################################################################
###################### Riconfiguro e metto nuovi file ##########################
################################################################################

# Verifico installazione di git, necessario per scaricare e gestire i nuovi script
sudo apt update
sudo apt upgrade
sudo apt install git

#
# Creo nuova cartella di configurazione.
# Sposto il labconfig.sh vecchio esistente più in alto nell'albero
# affinché non venga eliminato dal rm -rf successivo.
# Se lo script è già stato lanciato una volta,
# il labconfig sarà già stato posto in $config_dir/
# e devo evitare di perdere anch'esso
cp "${old_script_dir}/labconfig.sh" "/opt/labconfig.sh"
cp "${config_dir}/labconfig.sh" "/opt/labconfig.sh"


# Rimuovo vecchi file script e cartelle
rm -rf "${old_script_dir}"
rm -rf /opt/utils

# Copio vecchia configurazione al suo nuovo posto
mkdir -p "${openscuola_main_dir}"
mkdir -p "${config_dir}"
cp /opt/labconfig.sh "${config_dir}/labconfig.sh"
rm /opt/labconfig.sh

# Scarico nuovi repository
cd "${openscuola_main_dir}"
git clone -b 18_04 https://gitlab.com/openscuola/lab-configuration-scripts.git
chmod 744 "${script_dir}"/*.sh

git clone https://gitlab.com/openscuola/modelli-libreoffice.git


######################################################
# Rimuovo apt-mark hold da chromium-browser
# e da google-chrome-stable
######################################################
apt-mark unhold google-chrome-stable
apt-mark unhold chromium-browser


#######################################################
# Modifico autostart puntandoli alla
# nuova posizione di
# epoptes_reset_config.sh e desktop_icon_position.sh
#######################################################
# Devo importare le variabili di configurazione del laboratorio
# per sapere quanti utenti studente ci sono.
# A volte può contenere un \r\n invece di \n quando scaricato da windows,
# elimino il \r "carriage return" prima di importarlo.
cat "${config_dir}/labconfig.sh" | tr -d '\r' > "${config_dir}/tmp.txt"
mv "${config_dir}/tmp.txt" "${config_dir}/labconfig.sh"
# Importo il file di configurazione ripulito
source "${config_dir}/labconfig.sh"


#### Modifica su utenti "docente"
# Ordinamento icone ad ogni avvio della sessione
mkdir -p "/home/${username_docente}/.config/autostart"
chmod 755 "/home/${username_docente}/.config/autostart"
cat > "/home/${username_docente}/.config/autostart/desktop_icon_position.desktop" << EOF
[Desktop Entry]
Type=Application
Name=Desktop Icon Position
Exec=${script_dir}/desktop_icon_position.sh
EOF
chmod 755 "/home/${username_docente}/.config/autostart/desktop_icon_position.desktop"

# Reset configurazione epoptes ad ogni avvio della sessione
cat > "/home/${username_docente}/.config/autostart/epoptes_reset_config.desktop" << EOF
[Desktop Entry]
Type=Application
Name=Epoptes reset config
Exec=${script_dir}/epoptes_reset_config.sh
EOF
chmod 755 "/home/${username_docente}/.config/autostart/epoptes_reset_config.desktop"


#### Modifica su utenti "studenteXX"
for i in $(seq ${n_utenti}); do
  username_client="${username_client_base}$(printf "%02d" ${i})"
  # Ordinamento icone ad ogni avvio della sessione
  mkdir -p "/home/${username_client}/.config/autostart"
  chmod 755 "/home/${username_client}/.config/autostart"
  cat > "/home/${username_client}/.config/autostart/desktop_icon_position.desktop" << EOF
[Desktop Entry]
Type=Application
Name=Desktop Icon Position
Exec=${script_dir}/desktop_icon_position.sh
EOF
  chmod 755 "/home/${username_client}/.config/autostart/desktop_icon_position.desktop"
done